</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
  <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
<!-- Main Footer -->
<footer class="main-footer">
  <strong>Copyright &copy; 2020-2020 <a href="http://aswdc.in/"><?php echo "ARCH DAILY";?></a>.</strong>
  All rights reserved.
  <div class="float-right d-none d-sm-inline-block">
    <b>Version</b> 3.0.4
  </div>
</footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
</body>
</html>
<script>
 function toastMsg(){
      var toastmsg = "<?php echo $this->session->flashdata('login_success'); ?>";
      if(toastmsg != "" && toastmsg != undefined && toastmsg != null){
        toastr.success(toastmsg);
      }
    }
    function actionSuccessToast(){
      var toastmsg = "<?php echo $this->session->flashdata('success_msg'); ?>";
      if(toastmsg != "" && toastmsg != undefined && toastmsg != null){
        toastr.success(toastmsg);
      }
    }
    function actionErrorToast(){
      var toastmsg = "<?php echo $this->session->flashdata('error_msg'); ?>";
      if(toastmsg != "" && toastmsg != undefined && toastmsg != null){
        toastr.error(toastmsg);
      }
    }
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
  $(document).ready(function(){
      toastMsg();
      actionSuccessToast();
      actionErrorToast();
    });
</script>