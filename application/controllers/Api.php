<?php
defined('BASEPATH') or exit('No direct script access allowed');
//  header('Access-Control-Allow-Origin: *'); 

class Api extends MY_Controller
{

    public $resArr = array(
        CON_RES_CODE => CON_CODE_FAIL,
        CON_RES_MESSAGE => "",
        CON_RES_DATA => array(),
    );
    public function __construct()
    {
        parent::__construct();
     
        $this->load->model('Api_Model', 'AM');
    }

    public function getFestivals(){
        $festival_id = $this->input->post('fest_id');
        if(!empty($festival_id)){
            $festData = $this->AM->getFestData($festival_id);
            foreach ($festData as $key => $value) {
                $abc = explode('-',$festData[$key]['date']);
                $day = $abc[0];
                $month = $abc[1];
                if($month == '01'){
                    $text = 'January';
                    $festData[$key]['m_date'] = $text.'-'.$day;
                }
                if($month == '02'){
                    $text = 'February';
                    $festData[$key]['m_date'] = $text.'-'.$day;
                }
                if($month == '03'){
                    $text = 'March';
                    $festData[$key]['m_date'] = $text.'-'.$day;
                }
                if($month == '04'){
                    $text = 'April';
                    $festData[$key]['m_date'] = $text.'-'.$day;
                }
                if($month == '05'){
                    $text = 'May';
                    $festData[$key]['m_date'] = $text.'-'.$day;
                }
                if($month == '06'){
                    $text = 'June';
                    $festData[$key]['m_date'] = $text.'-'.$day;
                }
                if($month == '07'){
                    $text = 'July';
                    $festData[$key]['m_date'] = $text.'-'.$day;
                }
                if($month == '08'){
                    $text = 'August';
                    $festData[$key]['m_date'] = $text.'-'.$day;
                }
                if($month == '09'){
                    $text = 'September';
                    $festData[$key]['m_date'] = $text.'-'.$day;
                }
                if($month == '10'){
                    $text = 'October';
                    $festData[$key]['m_date'] = $text.'-'.$day;
                }
                if($month == '11'){
                    $text = 'November';
                    $festData[$key]['m_date'] = $text.'-'.$day;
                }
                if($month == '12'){
                    $text = 'December';
                    $festData[$key]['m_date'] = $text.'-'.$day;
                }
                
            }
            if($festData){
                $this->resArr[CON_RES_CODE] = CON_CODE_SUCCESS;
                $this->resArr[CON_RES_MESSAGE] = lang('MSG_GOT');
                $this->resArr[CON_RES_DATA] = array('festival_details' => $festData);
    
            }else{
                $this->resArr[CON_RES_MESSAGE] = lang('MSG_FAIL');
    
            }
        }else{
            $this->resArr[CON_RES_MESSAGE] = lang('INVALID_PARAMETER');

        }
       
        $this->sendResponse($this->resArr);

    }

    public function getTodayData(){
        $current_date = date('d-m');
        
        $data = $this->AM->getDetails($current_date);
        if($data){
            $this->resArr[CON_RES_CODE] = CON_CODE_SUCCESS;
            $this->resArr[CON_RES_MESSAGE] = lang('MSG_GOT');
            $this->resArr[CON_RES_DATA] = array('festival_details' => $data);

        }else{
            $this->resArr[CON_RES_MESSAGE] = lang('MSG_FAIL');

        }
        $this->sendResponse($this->resArr);

        
    }
    
}