<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Api_Model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getFestData($festival_id){

    $sql = "SELECT * FROM tbl_festival WHERE fes_id = '{$festival_id}'";
    $result = $this->db->query($sql);
    return $this->returnRows($result);
    }

    public function getDetails($current_date){
        $sql = "SELECT * FROM tbl_festival WHERE date = '{$current_date}'";
        $result = $this->db->query($sql);
        return $this->returnRows($result);
    }
}